//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppQuanLyDiemSV
{
    using System;
    using System.Collections.Generic;
    
    public partial class Giang_Khoa
    {
        public string Ma_CT { get; set; }
        public string Ma_Khoa { get; set; }
        public string MaMH { get; set; }
        public int Nam_Hoc { get; set; }
        public Nullable<int> Hoc_Ky { get; set; }
        public Nullable<int> STLT { get; set; }
        public Nullable<int> STTH { get; set; }
        public Nullable<int> So_Tin_Chi { get; set; }
    
        public virtual Chuong_Trinh_Hoc Chuong_Trinh_Hoc { get; set; }
        public virtual Khoa Khoa { get; set; }
        public virtual Mon_Hoc Mon_Hoc { get; set; }
    }
}
