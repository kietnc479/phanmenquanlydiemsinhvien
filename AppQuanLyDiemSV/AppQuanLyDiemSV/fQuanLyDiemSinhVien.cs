﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppQuanLyDiemSV
{
    public partial class fQuanLyDiemSinhVien : Form
    {
        Quan_Ly_Sinh_VienEntities db = new Quan_Ly_Sinh_VienEntities();
                 
        public fQuanLyDiemSinhVien()
        {
            InitializeComponent();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        private void label41_Click(object sender, EventArgs e)
        {

        }

        private void fQuanLyDiemSinhVien_Load(object sender, EventArgs e)
        {
            
        }
         

        private void dgvSinhVien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) 
            {
                DataGridViewRow row = this.dgvSinhVien.Rows[e.RowIndex];
                txbMSVSV.Text = row.Cells[0].Value.ToString();
                txbTenSV.Text = row.Cells[1].Value.ToString();
                txbNS.Text = row.Cells[2].Value.ToString();
                txbDanToc.Text = row.Cells[3].Value.ToString();
                txbML.Text = row.Cells[4].Value.ToString();
            }
        }

        private void btnThemSV_Click(object sender, EventArgs e)
        {
            Sinh_Vien sv = new Sinh_Vien();
            sv.MaSV = txbMSVSV.Text;
            sv.Ho_Ten =txbTenSV.Text;
            sv.Nam_Sinh = int.Parse(txbNS.Text);
            sv.Dan_Toc = txbDanToc.Text;
            sv.Ma_Lop = txbML.Text;
            db.Sinh_Vien.Add(sv);
            db.SaveChanges();


        }
        
        private void btnXem_Click(object sender, EventArgs e)
        {
            dgvSinhVien.DataSource = db.Sinh_Vien.Select(s => new { s.MaSV, s.Ho_Ten, s.Nam_Sinh, s.Dan_Toc, s.Ma_Lop }).ToList();
             
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnXemL_Click(object sender, EventArgs e)
        {
            dgvLop.DataSource = db.Lops.Select(l => new { l.Ma_Lop, l.Ma_Khoa, l.Ma_Khoa_Hoc, l.Ma_CT, l.STT }).ToList();
            
        }

        private void btnXemMH_Click(object sender, EventArgs e)
        {
            dgvMH.DataSource = db.Mon_Hoc.Select(m => new { m.MaMH, m.Ma_Khoa, m.TenMH }).ToList();
        }

        private void btnXemSV_Click(object sender, EventArgs e)
        {
            dgvBD.DataSource = db.Ket_Qua.Select(b => new { b.MaSV, b.MaMH, b.Lan_Thi, b.Diem_Thi }).ToList();
        }

        private void btnXemK_Click(object sender, EventArgs e)
        {
            dgvK.DataSource = db.Khoas.Select(k => new { k.Ma_Khoa, k.Ten_Khoa, k.Nam_Thanh_Lap }).ToList();
        }

        private void btnXemKH_Click(object sender, EventArgs e)
        {
            dgvKH.DataSource = db.Khoa_Hoc.Select(kh => new { kh.Ma_Khoa_Hoc, kh.Nam_Bat_Dau, kh.Nam_Ket_Thuc }).ToList();
        }

        private void btnXemGD_Click(object sender, EventArgs e)
        {
            dgvGD.DataSource = db.Giang_Khoa.Select(g => new { g.Ma_CT, g.Ma_Khoa, g.MaMH, g.Nam_Hoc, g.Hoc_Ky, g.STLT, g.STTH, g.So_Tin_Chi }).ToList();
        }

        private void btnSuaSV_Click(object sender, EventArgs e)
        {
            var sv = db.Sinh_Vien.Single(s => s.MaSV == txbMSVSV.Text);
            sv.MaSV = txbMSVSV.Text;
            sv.Ho_Ten = txbTenSV.Text;
            sv.Nam_Sinh = int.Parse(txbNS.Text);
            sv.Dan_Toc = txbDanToc.Text;
            sv.Ma_Lop = txbML.Text;
            db.SaveChanges();
        }
       

        private void btnLuuSV_Click(object sender, EventArgs e)
        {
             this.Close();
             Application.Exit();
        }

        private void dgvLop_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvLop.Rows[e.RowIndex];
                txbML1.Text = row.Cells[0].Value.ToString();
                txbMK.Text = row.Cells[1].Value.ToString();
                txbMaKhoa.Text = row.Cells[2].Value.ToString();
                txbCTH.Text = row.Cells[3].Value.ToString();
                txbSTTL.Text = row.Cells[4].Value.ToString();
            }
        }

        private void dgvMH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvMH.Rows[e.RowIndex];
                txbMMH.Text = row.Cells[0].Value.ToString();
                txbMKMH.Text = row.Cells[1].Value.ToString();
                txbTMH.Text = row.Cells[2].Value.ToString();
                 
            }
        }

        private void dgvBD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvBD.Rows[e.RowIndex];
                txbMSVBD.Text = row.Cells[0].Value.ToString();
                txbMaMHBD.Text = row.Cells[1].Value.ToString();
                txbLTBD.Text = row.Cells[2].Value.ToString();
                txbDiemthiBD.Text = row.Cells[3].Value.ToString();
                 
            }
        }

        private void dgvK_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvK.Rows[e.RowIndex];
                txbKK.Text = row.Cells[0].Value.ToString();
                txbTKK.Text = row.Cells[1].Value.ToString();
                txbNTL.Text = row.Cells[2].Value.ToString();
                
            }
        }

        private void dgvKH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvKH.Rows[e.RowIndex];
                txbMKH.Text = row.Cells[0].Value.ToString();
                txbNBD.Text = row.Cells[1].Value.ToString();
                txbNKT.Text = row.Cells[2].Value.ToString();
                 
            }
        }

        private void dgvGD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvGD.Rows[e.RowIndex];
                txbMCT.Text = row.Cells[0].Value.ToString();
                txbMKGD.Text = row.Cells[1].Value.ToString();
                txbMHHGD.Text = row.Cells[2].Value.ToString();
                txbNH.Text = row.Cells[3].Value.ToString();
                txbHK.Text = row.Cells[4].Value.ToString();
                txbSTLT.Text = row.Cells[5].Value.ToString();
                txbSTTH.Text = row.Cells[6].Value.ToString();
                txbSTC.Text = row.Cells[7].Value.ToString();

            }
        }

        private void btnXoaSV_Click(object sender, EventArgs e)
        {
            var sv = db.Sinh_Vien.Single(s => s.MaSV == txbMSVSV.Text);
            db.Sinh_Vien.Remove(sv);
            db.SaveChanges();
        }

        private void btnThemLop_Click(object sender, EventArgs e)
        {
            Lop l = new Lop();
            l.Ma_Lop = txbML1.Text;
            l.Ma_Khoa = txbMK.Text;
            l.Ma_Khoa_Hoc = txbMaKhoa.Text;
            l.Ma_CT= txbCTH.Text;
            l.STT = int.Parse(txbSTTL.Text);
            db.Lops.Add(l);
            db.SaveChanges();

        }

        private void btnSuaLop_Click(object sender, EventArgs e)
        {
            var l = db.Lops.Single(s => s.Ma_Lop == txbML1.Text);
            l.Ma_Khoa = txbMK.Text;
            l.Ma_Khoa_Hoc = txbMaKhoa.Text;
            l.Ma_CT = txbCTH.Text;
            l.STT = int.Parse(txbSTTL.Text);
            db.SaveChanges();

        }

        private void btnThemMH_Click(object sender, EventArgs e)
        {
            Mon_Hoc mh = new Mon_Hoc();
            mh.MaMH = txbMMH.Text;
            mh.TenMH = txbTMH.Text;
            mh.Ma_Khoa = txbMKMH.Text;
            db.Mon_Hoc.Add(mh);
            db.SaveChanges();

        }

        private void btnSuaMH_Click(object sender, EventArgs e)
        {
            var mh = db.Mon_Hoc.Single(m => m.MaMH == txbMMH.Text);
            mh.TenMH = txbTMH.Text;
            mh.Ma_Khoa = txbMKMH.Text;
            db.SaveChanges();
        }

        private void btnXoaMH_Click(object sender, EventArgs e)
        {
            var mh = db.Mon_Hoc.Single(m => m.MaMH == txbMMH.Text);
            db.Mon_Hoc.Remove(mh);
            db.SaveChanges();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Ket_Qua kq = new Ket_Qua();
            kq.MaSV = txbMSVBD.Text;
            kq.MaMH = txbMaMHBD.Text;
            kq.Lan_Thi = int.Parse(txbLTBD.Text);
            kq.Diem_Thi = int.Parse(txbDiemthiBD.Text);
            db.Ket_Qua.Add(kq);
            db.SaveChanges();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var kq = db.Ket_Qua.Single(k => k.MaSV == txbMSVBD.Text);
            //sv.MaSV = txbMSVSV.Text;
            kq.MaMH = txbMaMHBD.Text;
            kq.Lan_Thi = int.Parse(txbLTBD.Text);
            kq.Diem_Thi = int.Parse(txbDiemthiBD.Text);
            db.SaveChanges();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var kq = db.Ket_Qua.Single(k => k.MaSV == txbMSVBD.Text);
            db.Ket_Qua.Remove(kq);
            db.SaveChanges();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Khoa k = new Khoa();
            k.Ma_Khoa = txbKK.Text;
            k.Ten_Khoa = txbTKK.Text;
            k.Nam_Thanh_Lap = int.Parse(txbNTL.Text);
            db.Khoas.Add(k);
            db.SaveChanges();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            var k = db.Khoas.Single(kh => kh.Ma_Khoa == txbKK.Text);
            k.Ten_Khoa = txbTKK.Text;
            k.Nam_Thanh_Lap = int.Parse(txbNTL.Text);
            db.SaveChanges();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var k = db.Khoas.Single(kh => kh.Ma_Khoa == txbKK.Text);
            db.Khoas.Remove(k);
            db.SaveChanges();
        }

        private void btnThemKH_Click(object sender, EventArgs e)
        {
            Khoa_Hoc kh = new Khoa_Hoc();
            kh.Ma_Khoa_Hoc = txbMKH.Text;
            kh.Nam_Bat_Dau = int.Parse(txbNBD.Text);
            kh.Nam_Ket_Thuc = int.Parse(txbNKT.Text);
            db.Khoa_Hoc.Add(kh);
            db.SaveChanges();
        }

        private void btnSuaKH_Click(object sender, EventArgs e)
        {
            var kh = db.Khoa_Hoc.Single(k => k.Ma_Khoa_Hoc == txbMKH.Text);
            kh.Nam_Bat_Dau = int.Parse(txbNBD.Text);
            kh.Nam_Ket_Thuc = int.Parse(txbNKT.Text);
            db.SaveChanges();
        }

        private void btnXoaKH_Click(object sender, EventArgs e)
        {
            var kh = db.Khoa_Hoc.Single(k => k.Ma_Khoa_Hoc == txbMKH.Text);
            db.Khoa_Hoc.Remove(kh);
            db.SaveChanges();
        }

        private void btnThemGD_Click(object sender, EventArgs e)
        {
            Giang_Khoa gk = new Giang_Khoa();
            gk.Ma_CT = txbMCT.Text;
            gk.Ma_Khoa = txbMKGD.Text;
            gk.MaMH = txbMHHGD.Text;
            gk.Nam_Hoc = int.Parse(txbNH.Text);
            gk.Hoc_Ky = int.Parse(txbHK.Text);
            gk.STLT = int.Parse(txbSTLT.Text);
            gk.STTH = int.Parse(txbSTTH.Text);
            gk.So_Tin_Chi = int.Parse(txbSTC.Text);
            db.Giang_Khoa.Add(gk);
            db.SaveChanges();
        }

        private void btnSuaGD_Click(object sender, EventArgs e)
        {
            var gk = db.Giang_Khoa.Single(g => g.Ma_CT == txbMCT.Text);
            gk.Ma_Khoa = txbMKGD.Text;
            gk.MaMH = txbMHHGD.Text;
            gk.Nam_Hoc = int.Parse(txbNH.Text);
            gk.Hoc_Ky = int.Parse(txbHK.Text);
            gk.STLT = int.Parse(txbSTLT.Text);
            gk.STTH = int.Parse(txbSTTH.Text);
            gk.So_Tin_Chi = int.Parse(txbSTC.Text);
            db.SaveChanges();
        }

        private void btnXoaGD_Click(object sender, EventArgs e)
        {
            var gk = db.Giang_Khoa.Single(g => g.Ma_CT == txbMCT.Text);
            db.Giang_Khoa.Remove(gk);
            db.SaveChanges();
        }

        private void btnThoatLop_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnThoatMH_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnThoatKH_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnThoatGD_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnXoaLop_Click(object sender, EventArgs e)
        {
            var l = db.Lops.Single(s => s.Ma_Lop == txbML1.Text);
            db.Lops.Remove(l);
            db.SaveChanges();
        }

        private void btnTimSV_Click(object sender, EventArgs e)
        {
             
        }

        private void txbTimSV_TextChanged(object sender, EventArgs e)
        {
            
            var t = (from s in db.Sinh_Vien where s.MaSV.Contains(txbTimSV.Text) select s).ToList();
            var te = (from a in db.Sinh_Vien where a.Ho_Ten.Contains(txbTimSV.Text) select a).ToList();
            dgvSinhVien.DataSource = t;
            dgvSinhVien.DataSource = te;
            txbMSVSV.DataBindings.Clear();
            txbTenSV.DataBindings.Clear();
            
            txbMSVSV.DataBindings.Add("text", t, "MaSV");
            txbTenSV.DataBindings.Add("text", te, "Ho_Ten");
             
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var tl = (from l in db.Lops where l.Ma_Lop.Contains(txbTimlop.Text) select l).ToList();
            dgvLop.DataSource = tl;
            txbML1.DataBindings.Clear();
            txbML1.DataBindings.Add("text", tl, "Ma_Lop");
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            var mm = (from m in db.Mon_Hoc where m.MaMH.Contains(textBox1.Text) select m).ToList();
            dgvMH.DataSource = mm;
            txbMMH.DataBindings.Clear();
            txbMMH.DataBindings.Add("text", mm, "MaMH");
        }
    }
}
