﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppQuanLyDiemSV
{
    public partial class DangNhap : Form
    {
        public DangNhap()
        {
            InitializeComponent();
        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void btnLogin_Click(object sender, EventArgs e)
        {
            fQuanLyDiemSinhVien f = new fQuanLyDiemSinhVien();
            
            if (txbUserName.Text.Length == 0 && txbPass.Text.Length == 0)
            {
                MessageBox.Show("Bạn chưa nhập tài khoản hoặc mật khẩu", "Thông báo");
            }
            else
                if (this.txbUserName.Text == "admin" && this.txbPass.Text == "huynhtankiet")
            {
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
                MessageBox.Show("Bạn đã nhập sai tài khoản hoặc mật khẩu","Thông báo");

        }

        private void DangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát chương trình?","Thông báo", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK) 
            {
                e.Cancel = true;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tài khoản:admin\nMật khẩu:huynhtankiet", "Gợi ý");
        }
    }
}
