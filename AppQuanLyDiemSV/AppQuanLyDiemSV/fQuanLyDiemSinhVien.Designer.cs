﻿
namespace AppQuanLyDiemSV
{
    partial class fQuanLyDiemSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabDiem = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnXem = new System.Windows.Forms.Button();
            this.txbML = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbDanToc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbNS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnThoatSV = new System.Windows.Forms.Button();
            this.btnXoaSV = new System.Windows.Forms.Button();
            this.btnSuaSV = new System.Windows.Forms.Button();
            this.btnThemSV = new System.Windows.Forms.Button();
            this.dgvSinhVien = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbTenSV = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbMSVSV = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnXemL = new System.Windows.Forms.Button();
            this.txbSTTL = new System.Windows.Forms.TextBox();
            this.txbSTT = new System.Windows.Forms.Label();
            this.txbCTH = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txbMaKhoa = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnThoatLop = new System.Windows.Forms.Button();
            this.btnXoaLop = new System.Windows.Forms.Button();
            this.btnSuaLop = new System.Windows.Forms.Button();
            this.btnThemLop = new System.Windows.Forms.Button();
            this.dgvLop = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbMK = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txbML1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnXemMH = new System.Windows.Forms.Button();
            this.txbTMH = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnThoatMH = new System.Windows.Forms.Button();
            this.btnXoaMH = new System.Windows.Forms.Button();
            this.btnSuaMH = new System.Windows.Forms.Button();
            this.btnThemMH = new System.Windows.Forms.Button();
            this.dgvMH = new System.Windows.Forms.DataGridView();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbMKMH = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txbMMH = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnXemSV = new System.Windows.Forms.Button();
            this.txbDiemthiBD = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txbLTBD = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dgvBD = new System.Windows.Forms.DataGridView();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbMaMHBD = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txbMSVBD = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnXemK = new System.Windows.Forms.Button();
            this.txbNTL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.dgvK = new System.Windows.Forms.DataGridView();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbTKK = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txbKK = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnXemKH = new System.Windows.Forms.Button();
            this.txbNKT = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btnThoatKH = new System.Windows.Forms.Button();
            this.btnXoaKH = new System.Windows.Forms.Button();
            this.btnSuaKH = new System.Windows.Forms.Button();
            this.btnThemKH = new System.Windows.Forms.Button();
            this.dgvKH = new System.Windows.Forms.DataGridView();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbNBD = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txbMKH = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.btnXemGD = new System.Windows.Forms.Button();
            this.txbSTC = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txbSTTH = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txbSTLT = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txbHK = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txbNH = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txbMHHGD = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.btnThoatGD = new System.Windows.Forms.Button();
            this.btnXoaGD = new System.Windows.Forms.Button();
            this.btnSuaGD = new System.Windows.Forms.Button();
            this.btnThemGD = new System.Windows.Forms.Button();
            this.dgvGD = new System.Windows.Forms.DataGridView();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txbMKGD = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txbMCT = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txbTimSV = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txbTimlop = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabDiem.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSinhVien)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLop)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMH)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBD)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvK)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKH)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGD)).BeginInit();
            this.SuspendLayout();
            // 
            // tabDiem
            // 
            this.tabDiem.Controls.Add(this.tabPage1);
            this.tabDiem.Controls.Add(this.tabPage2);
            this.tabDiem.Controls.Add(this.tabPage3);
            this.tabDiem.Controls.Add(this.tabPage4);
            this.tabDiem.Controls.Add(this.tabPage5);
            this.tabDiem.Controls.Add(this.tabPage6);
            this.tabDiem.Controls.Add(this.tabPage7);
            this.tabDiem.Controls.Add(this.tabPage8);
            this.tabDiem.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDiem.Location = new System.Drawing.Point(12, 12);
            this.tabDiem.Name = "tabDiem";
            this.tabDiem.SelectedIndex = 0;
            this.tabDiem.Size = new System.Drawing.Size(958, 579);
            this.tabDiem.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.maxresdefault;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(950, 543);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Trang chủ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(248, 69);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(463, 26);
            this.label41.TabIndex = 3;
            this.label41.Text = "PHẦN MỀM QUẢN LÝ ĐIỂM SINH VIÊN";
            this.label41.Click += new System.EventHandler(this.label41_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(625, 505);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(253, 23);
            this.label40.TabIndex = 2;
            this.label40.Text = "Email: kietnc479@gmai.com";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(625, 459);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(253, 23);
            this.label39.TabIndex = 1;
            this.label39.Text = "Mã số sinh viên: 1900008454";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(625, 411);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(294, 23);
            this.label38.TabIndex = 0;
            this.label38.Text = "Người thực hiện: Huỳnh Tấn Kiệt";
            this.label38.Click += new System.EventHandler(this.label38_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.txbTimSV);
            this.tabPage2.Controls.Add(this.btnXem);
            this.tabPage2.Controls.Add(this.txbML);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txbDanToc);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txbNS);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.btnThoatSV);
            this.tabPage2.Controls.Add(this.btnXoaSV);
            this.tabPage2.Controls.Add(this.btnSuaSV);
            this.tabPage2.Controls.Add(this.btnThemSV);
            this.tabPage2.Controls.Add(this.dgvSinhVien);
            this.tabPage2.Controls.Add(this.txbTenSV);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txbMSVSV);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(950, 543);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thông tin sinh viên";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // btnXem
            // 
            this.btnXem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXem.Location = new System.Drawing.Point(37, 483);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(124, 41);
            this.btnXem.TabIndex = 11;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // txbML
            // 
            this.txbML.Location = new System.Drawing.Point(647, 132);
            this.txbML.Name = "txbML";
            this.txbML.Size = new System.Drawing.Size(254, 30);
            this.txbML.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(499, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 23);
            this.label6.TabIndex = 9;
            this.label6.Text = "Mã lớp";
            // 
            // txbDanToc
            // 
            this.txbDanToc.Location = new System.Drawing.Point(647, 81);
            this.txbDanToc.Name = "txbDanToc";
            this.txbDanToc.Size = new System.Drawing.Size(254, 30);
            this.txbDanToc.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(499, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Dân tộc";
            // 
            // txbNS
            // 
            this.txbNS.Location = new System.Drawing.Point(191, 185);
            this.txbNS.Name = "txbNS";
            this.txbNS.Size = new System.Drawing.Size(254, 30);
            this.txbNS.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "Năm sinh";
            // 
            // btnThoatSV
            // 
            this.btnThoatSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoatSV.Location = new System.Drawing.Point(762, 483);
            this.btnThoatSV.Name = "btnThoatSV";
            this.btnThoatSV.Size = new System.Drawing.Size(124, 41);
            this.btnThoatSV.TabIndex = 15;
            this.btnThoatSV.Text = "Thoát";
            this.btnThoatSV.UseVisualStyleBackColor = true;
            this.btnThoatSV.Click += new System.EventHandler(this.btnLuuSV_Click);
            // 
            // btnXoaSV
            // 
            this.btnXoaSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaSV.Location = new System.Drawing.Point(574, 483);
            this.btnXoaSV.Name = "btnXoaSV";
            this.btnXoaSV.Size = new System.Drawing.Size(124, 41);
            this.btnXoaSV.TabIndex = 14;
            this.btnXoaSV.Text = "Xóa";
            this.btnXoaSV.UseVisualStyleBackColor = true;
            this.btnXoaSV.Click += new System.EventHandler(this.btnXoaSV_Click);
            // 
            // btnSuaSV
            // 
            this.btnSuaSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaSV.Location = new System.Drawing.Point(399, 483);
            this.btnSuaSV.Name = "btnSuaSV";
            this.btnSuaSV.Size = new System.Drawing.Size(124, 41);
            this.btnSuaSV.TabIndex = 13;
            this.btnSuaSV.Text = "Sửa";
            this.btnSuaSV.UseVisualStyleBackColor = true;
            this.btnSuaSV.Click += new System.EventHandler(this.btnSuaSV_Click);
            // 
            // btnThemSV
            // 
            this.btnThemSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemSV.Location = new System.Drawing.Point(218, 483);
            this.btnThemSV.Name = "btnThemSV";
            this.btnThemSV.Size = new System.Drawing.Size(124, 41);
            this.btnThemSV.TabIndex = 12;
            this.btnThemSV.Text = "Thêm";
            this.btnThemSV.UseVisualStyleBackColor = true;
            this.btnThemSV.Click += new System.EventHandler(this.btnThemSV_Click);
            // 
            // dgvSinhVien
            // 
            this.dgvSinhVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSinhVien.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgvSinhVien.Location = new System.Drawing.Point(6, 234);
            this.dgvSinhVien.Name = "dgvSinhVien";
            this.dgvSinhVien.RowHeadersWidth = 51;
            this.dgvSinhVien.RowTemplate.Height = 24;
            this.dgvSinhVien.Size = new System.Drawing.Size(938, 231);
            this.dgvSinhVien.TabIndex = 16;
            this.dgvSinhVien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSinhVien_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "MaSV";
            this.Column1.HeaderText = "Mã sinh viên";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 120;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Ho_Ten";
            this.Column2.HeaderText = "Họ tên";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Nam_Sinh";
            this.Column3.HeaderText = "Năm sinh";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 125;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Dan_Toc";
            this.Column4.HeaderText = "Dân tộc ";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Ma_Lop";
            this.Column5.HeaderText = "Mã lớp";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Width = 125;
            // 
            // txbTenSV
            // 
            this.txbTenSV.Location = new System.Drawing.Point(191, 132);
            this.txbTenSV.Name = "txbTenSV";
            this.txbTenSV.Size = new System.Drawing.Size(254, 30);
            this.txbTenSV.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tên sinh viên";
            // 
            // txbMSVSV
            // 
            this.txbMSVSV.Location = new System.Drawing.Point(191, 81);
            this.txbMSVSV.Name = "txbMSVSV";
            this.txbMSVSV.Size = new System.Drawing.Size(254, 30);
            this.txbMSVSV.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã sinh viên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(341, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN SINH VIÊN";
            // 
            // tabPage3
            // 
            this.tabPage3.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.Controls.Add(this.label43);
            this.tabPage3.Controls.Add(this.txbTimlop);
            this.tabPage3.Controls.Add(this.btnXemL);
            this.tabPage3.Controls.Add(this.txbSTTL);
            this.tabPage3.Controls.Add(this.txbSTT);
            this.tabPage3.Controls.Add(this.txbCTH);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txbMaKhoa);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.btnThoatLop);
            this.tabPage3.Controls.Add(this.btnXoaLop);
            this.tabPage3.Controls.Add(this.btnSuaLop);
            this.tabPage3.Controls.Add(this.btnThemLop);
            this.tabPage3.Controls.Add(this.dgvLop);
            this.tabPage3.Controls.Add(this.txbMK);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.txbML1);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(950, 543);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Danh sách lớp";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // btnXemL
            // 
            this.btnXemL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemL.Location = new System.Drawing.Point(48, 482);
            this.btnXemL.Name = "btnXemL";
            this.btnXemL.Size = new System.Drawing.Size(124, 41);
            this.btnXemL.TabIndex = 30;
            this.btnXemL.Text = "Xem";
            this.btnXemL.UseVisualStyleBackColor = true;
            this.btnXemL.Click += new System.EventHandler(this.btnXemL_Click);
            // 
            // txbSTTL
            // 
            this.txbSTTL.Location = new System.Drawing.Point(648, 131);
            this.txbSTTL.Name = "txbSTTL";
            this.txbSTTL.Size = new System.Drawing.Size(254, 30);
            this.txbSTTL.TabIndex = 29;
            // 
            // txbSTT
            // 
            this.txbSTT.AutoSize = true;
            this.txbSTT.Location = new System.Drawing.Point(500, 138);
            this.txbSTT.Name = "txbSTT";
            this.txbSTT.Size = new System.Drawing.Size(88, 23);
            this.txbSTT.TabIndex = 28;
            this.txbSTT.Text = "Số thứ tự";
            // 
            // txbCTH
            // 
            this.txbCTH.Location = new System.Drawing.Point(648, 80);
            this.txbCTH.Name = "txbCTH";
            this.txbCTH.Size = new System.Drawing.Size(254, 30);
            this.txbCTH.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(482, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 23);
            this.label8.TabIndex = 26;
            this.label8.Text = "Chương trình học";
            // 
            // txbMaKhoa
            // 
            this.txbMaKhoa.Location = new System.Drawing.Point(192, 184);
            this.txbMaKhoa.Name = "txbMaKhoa";
            this.txbMaKhoa.Size = new System.Drawing.Size(254, 30);
            this.txbMaKhoa.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 23);
            this.label9.TabIndex = 24;
            this.label9.Text = "Mã khóa";
            // 
            // btnThoatLop
            // 
            this.btnThoatLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoatLop.Location = new System.Drawing.Point(778, 482);
            this.btnThoatLop.Name = "btnThoatLop";
            this.btnThoatLop.Size = new System.Drawing.Size(124, 41);
            this.btnThoatLop.TabIndex = 34;
            this.btnThoatLop.Text = "Thoát";
            this.btnThoatLop.UseVisualStyleBackColor = true;
            this.btnThoatLop.Click += new System.EventHandler(this.btnThoatLop_Click);
            // 
            // btnXoaLop
            // 
            this.btnXoaLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLop.Location = new System.Drawing.Point(596, 482);
            this.btnXoaLop.Name = "btnXoaLop";
            this.btnXoaLop.Size = new System.Drawing.Size(124, 41);
            this.btnXoaLop.TabIndex = 33;
            this.btnXoaLop.Text = "Xóa";
            this.btnXoaLop.UseVisualStyleBackColor = true;
            this.btnXoaLop.Click += new System.EventHandler(this.btnXoaLop_Click);
            // 
            // btnSuaLop
            // 
            this.btnSuaLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaLop.Location = new System.Drawing.Point(411, 482);
            this.btnSuaLop.Name = "btnSuaLop";
            this.btnSuaLop.Size = new System.Drawing.Size(124, 41);
            this.btnSuaLop.TabIndex = 32;
            this.btnSuaLop.Text = "Sửa";
            this.btnSuaLop.UseVisualStyleBackColor = true;
            this.btnSuaLop.Click += new System.EventHandler(this.btnSuaLop_Click);
            // 
            // btnThemLop
            // 
            this.btnThemLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLop.Location = new System.Drawing.Point(232, 482);
            this.btnThemLop.Name = "btnThemLop";
            this.btnThemLop.Size = new System.Drawing.Size(124, 41);
            this.btnThemLop.TabIndex = 31;
            this.btnThemLop.Text = "Thêm";
            this.btnThemLop.UseVisualStyleBackColor = true;
            this.btnThemLop.Click += new System.EventHandler(this.btnThemLop_Click);
            // 
            // dgvLop
            // 
            this.dgvLop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dgvLop.Location = new System.Drawing.Point(7, 233);
            this.dgvLop.Name = "dgvLop";
            this.dgvLop.RowHeadersWidth = 51;
            this.dgvLop.RowTemplate.Height = 24;
            this.dgvLop.Size = new System.Drawing.Size(938, 231);
            this.dgvLop.TabIndex = 35;
            this.dgvLop.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLop_CellContentClick);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Ma_Lop";
            this.Column6.HeaderText = "Mã lớp";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.Width = 125;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Ma_Khoa";
            this.Column7.HeaderText = "Mã khoa";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.Width = 125;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Ma_Khoa_Hoc";
            this.Column8.HeaderText = "Mã khóa học";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.Width = 125;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Ma_CT";
            this.Column9.HeaderText = "Mã chương trình ";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.Width = 125;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "STT";
            this.Column10.HeaderText = "Số thứ tự";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.Width = 125;
            // 
            // txbMK
            // 
            this.txbMK.Location = new System.Drawing.Point(192, 131);
            this.txbMK.Name = "txbMK";
            this.txbMK.Size = new System.Drawing.Size(254, 30);
            this.txbMK.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 23);
            this.label11.TabIndex = 22;
            this.label11.Text = "Mã khoa";
            // 
            // txbML1
            // 
            this.txbML1.Location = new System.Drawing.Point(192, 80);
            this.txbML1.Name = "txbML1";
            this.txbML1.Size = new System.Drawing.Size(254, 30);
            this.txbML1.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(44, 87);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 23);
            this.label12.TabIndex = 20;
            this.label12.Text = "Mã lớp";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(187, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(582, 26);
            this.label10.TabIndex = 14;
            this.label10.Text = "DANH SÁCH LỚP HỌC PHẦN HỌC KỲ 1 NĂM 2023";
            // 
            // tabPage4
            // 
            this.tabPage4.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage4.Controls.Add(this.label44);
            this.tabPage4.Controls.Add(this.textBox1);
            this.tabPage4.Controls.Add(this.btnXemMH);
            this.tabPage4.Controls.Add(this.txbTMH);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.btnThoatMH);
            this.tabPage4.Controls.Add(this.btnXoaMH);
            this.tabPage4.Controls.Add(this.btnSuaMH);
            this.tabPage4.Controls.Add(this.btnThemMH);
            this.tabPage4.Controls.Add(this.dgvMH);
            this.tabPage4.Controls.Add(this.txbMKMH);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.txbMMH);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Location = new System.Drawing.Point(4, 32);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(950, 543);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Môn học";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnXemMH
            // 
            this.btnXemMH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemMH.Location = new System.Drawing.Point(57, 465);
            this.btnXemMH.Name = "btnXemMH";
            this.btnXemMH.Size = new System.Drawing.Size(124, 41);
            this.btnXemMH.TabIndex = 42;
            this.btnXemMH.Text = "Xem";
            this.btnXemMH.UseVisualStyleBackColor = true;
            this.btnXemMH.Click += new System.EventHandler(this.btnXemMH_Click);
            // 
            // txbTMH
            // 
            this.txbTMH.Location = new System.Drawing.Point(647, 77);
            this.txbTMH.Name = "txbTMH";
            this.txbTMH.Size = new System.Drawing.Size(254, 30);
            this.txbTMH.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(499, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 23);
            this.label14.TabIndex = 40;
            this.label14.Text = "Tên môn học";
            // 
            // btnThoatMH
            // 
            this.btnThoatMH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoatMH.Location = new System.Drawing.Point(767, 465);
            this.btnThoatMH.Name = "btnThoatMH";
            this.btnThoatMH.Size = new System.Drawing.Size(124, 41);
            this.btnThoatMH.TabIndex = 46;
            this.btnThoatMH.Text = "Thoát";
            this.btnThoatMH.UseVisualStyleBackColor = true;
            this.btnThoatMH.Click += new System.EventHandler(this.btnThoatMH_Click);
            // 
            // btnXoaMH
            // 
            this.btnXoaMH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaMH.Location = new System.Drawing.Point(590, 465);
            this.btnXoaMH.Name = "btnXoaMH";
            this.btnXoaMH.Size = new System.Drawing.Size(124, 41);
            this.btnXoaMH.TabIndex = 45;
            this.btnXoaMH.Text = "Xóa";
            this.btnXoaMH.UseVisualStyleBackColor = true;
            this.btnXoaMH.Click += new System.EventHandler(this.btnXoaMH_Click);
            // 
            // btnSuaMH
            // 
            this.btnSuaMH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaMH.Location = new System.Drawing.Point(412, 465);
            this.btnSuaMH.Name = "btnSuaMH";
            this.btnSuaMH.Size = new System.Drawing.Size(124, 41);
            this.btnSuaMH.TabIndex = 44;
            this.btnSuaMH.Text = "Sửa";
            this.btnSuaMH.UseVisualStyleBackColor = true;
            this.btnSuaMH.Click += new System.EventHandler(this.btnSuaMH_Click);
            // 
            // btnThemMH
            // 
            this.btnThemMH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemMH.Location = new System.Drawing.Point(230, 465);
            this.btnThemMH.Name = "btnThemMH";
            this.btnThemMH.Size = new System.Drawing.Size(124, 41);
            this.btnThemMH.TabIndex = 43;
            this.btnThemMH.Text = "Thêm";
            this.btnThemMH.UseVisualStyleBackColor = true;
            this.btnThemMH.Click += new System.EventHandler(this.btnThemMH_Click);
            // 
            // dgvMH
            // 
            this.dgvMH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12,
            this.Column13});
            this.dgvMH.Location = new System.Drawing.Point(6, 209);
            this.dgvMH.Name = "dgvMH";
            this.dgvMH.RowHeadersWidth = 51;
            this.dgvMH.RowTemplate.Height = 24;
            this.dgvMH.Size = new System.Drawing.Size(938, 231);
            this.dgvMH.TabIndex = 47;
            this.dgvMH.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMH_CellContentClick);
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "MaMH";
            this.Column11.HeaderText = "Mã môn học";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.Width = 125;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "Ma_Khoa";
            this.Column12.HeaderText = "Mã khoa";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.Width = 125;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "TenMH";
            this.Column13.HeaderText = "Tên môn học";
            this.Column13.MinimumWidth = 6;
            this.Column13.Name = "Column13";
            this.Column13.Width = 160;
            // 
            // txbMKMH
            // 
            this.txbMKMH.Location = new System.Drawing.Point(191, 128);
            this.txbMKMH.Name = "txbMKMH";
            this.txbMKMH.Size = new System.Drawing.Size(254, 30);
            this.txbMKMH.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(43, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 23);
            this.label15.TabIndex = 38;
            this.label15.Text = "Mã khoa";
            // 
            // txbMMH
            // 
            this.txbMMH.Location = new System.Drawing.Point(191, 77);
            this.txbMMH.Name = "txbMMH";
            this.txbMMH.Size = new System.Drawing.Size(254, 30);
            this.txbMMH.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(43, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 23);
            this.label16.TabIndex = 36;
            this.label16.Text = "Mã môn học";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(304, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(286, 26);
            this.label17.TabIndex = 35;
            this.label17.Text = "DANH SÁCH MÔN HỌC ";
            // 
            // tabPage5
            // 
            this.tabPage5.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage5.Controls.Add(this.btnXemSV);
            this.tabPage5.Controls.Add(this.txbDiemthiBD);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.txbLTBD);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Controls.Add(this.button2);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.dgvBD);
            this.tabPage5.Controls.Add(this.txbMaMHBD);
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.txbMSVBD);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Location = new System.Drawing.Point(4, 32);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(950, 543);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Bảng điểm";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnXemSV
            // 
            this.btnXemSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemSV.Location = new System.Drawing.Point(60, 463);
            this.btnXemSV.Name = "btnXemSV";
            this.btnXemSV.Size = new System.Drawing.Size(124, 41);
            this.btnXemSV.TabIndex = 29;
            this.btnXemSV.Text = "Xem";
            this.btnXemSV.UseVisualStyleBackColor = true;
            this.btnXemSV.Click += new System.EventHandler(this.btnXemSV_Click);
            // 
            // txbDiemthiBD
            // 
            this.txbDiemthiBD.Location = new System.Drawing.Point(647, 127);
            this.txbDiemthiBD.Name = "txbDiemthiBD";
            this.txbDiemthiBD.Size = new System.Drawing.Size(254, 30);
            this.txbDiemthiBD.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(499, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 23);
            this.label13.TabIndex = 27;
            this.label13.Text = "Điểm thi";
            // 
            // txbLTBD
            // 
            this.txbLTBD.Location = new System.Drawing.Point(647, 76);
            this.txbLTBD.Name = "txbLTBD";
            this.txbLTBD.Size = new System.Drawing.Size(254, 30);
            this.txbLTBD.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(499, 83);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 23);
            this.label18.TabIndex = 25;
            this.label18.Text = "Lần thi";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(766, 463);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 41);
            this.button1.TabIndex = 33;
            this.button1.Text = "Thoát";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(588, 463);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 41);
            this.button2.TabIndex = 32;
            this.button2.Text = "Xóa";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(412, 463);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 41);
            this.button3.TabIndex = 31;
            this.button3.Text = "Sửa";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(231, 463);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(124, 41);
            this.button4.TabIndex = 30;
            this.button4.Text = "Thêm";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dgvBD
            // 
            this.dgvBD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17});
            this.dgvBD.Location = new System.Drawing.Point(6, 201);
            this.dgvBD.Name = "dgvBD";
            this.dgvBD.RowHeadersWidth = 51;
            this.dgvBD.RowTemplate.Height = 24;
            this.dgvBD.Size = new System.Drawing.Size(938, 231);
            this.dgvBD.TabIndex = 34;
            this.dgvBD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBD_CellContentClick);
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "MaSV";
            this.Column14.HeaderText = "Mã sinh viên";
            this.Column14.MinimumWidth = 6;
            this.Column14.Name = "Column14";
            this.Column14.Width = 125;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "MaMH";
            this.Column15.HeaderText = "Mã môn học";
            this.Column15.MinimumWidth = 6;
            this.Column15.Name = "Column15";
            this.Column15.Width = 125;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Lan_Thi";
            this.Column16.HeaderText = "Lần thi";
            this.Column16.MinimumWidth = 6;
            this.Column16.Name = "Column16";
            this.Column16.Width = 125;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "Diem_Thi";
            this.Column17.HeaderText = "Điểm thi";
            this.Column17.MinimumWidth = 6;
            this.Column17.Name = "Column17";
            this.Column17.Width = 125;
            // 
            // txbMaMHBD
            // 
            this.txbMaMHBD.Location = new System.Drawing.Point(191, 127);
            this.txbMaMHBD.Name = "txbMaMHBD";
            this.txbMaMHBD.Size = new System.Drawing.Size(254, 30);
            this.txbMaMHBD.TabIndex = 24;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(43, 134);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(115, 23);
            this.label19.TabIndex = 23;
            this.label19.Text = "Mã môn học";
            // 
            // txbMSVBD
            // 
            this.txbMSVBD.Location = new System.Drawing.Point(191, 76);
            this.txbMSVBD.Name = "txbMSVBD";
            this.txbMSVBD.Size = new System.Drawing.Size(254, 30);
            this.txbMSVBD.TabIndex = 22;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(43, 83);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(118, 23);
            this.label20.TabIndex = 21;
            this.label20.Text = "Mã sinh viên";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(396, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(151, 26);
            this.label21.TabIndex = 20;
            this.label21.Text = "BẢNG ĐIỂM";
            // 
            // tabPage6
            // 
            this.tabPage6.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage6.Controls.Add(this.btnXemK);
            this.tabPage6.Controls.Add(this.txbNTL);
            this.tabPage6.Controls.Add(this.label7);
            this.tabPage6.Controls.Add(this.button5);
            this.tabPage6.Controls.Add(this.button6);
            this.tabPage6.Controls.Add(this.button7);
            this.tabPage6.Controls.Add(this.button8);
            this.tabPage6.Controls.Add(this.dgvK);
            this.tabPage6.Controls.Add(this.txbTKK);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.Controls.Add(this.txbKK);
            this.tabPage6.Controls.Add(this.label23);
            this.tabPage6.Controls.Add(this.label24);
            this.tabPage6.Location = new System.Drawing.Point(4, 32);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(950, 543);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Khoa";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btnXemK
            // 
            this.btnXemK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemK.Location = new System.Drawing.Point(59, 471);
            this.btnXemK.Name = "btnXemK";
            this.btnXemK.Size = new System.Drawing.Size(124, 41);
            this.btnXemK.TabIndex = 54;
            this.btnXemK.Text = "Xem";
            this.btnXemK.UseVisualStyleBackColor = true;
            this.btnXemK.Click += new System.EventHandler(this.btnXemK_Click);
            // 
            // txbNTL
            // 
            this.txbNTL.Location = new System.Drawing.Point(647, 83);
            this.txbNTL.Name = "txbNTL";
            this.txbNTL.Size = new System.Drawing.Size(254, 30);
            this.txbNTL.TabIndex = 53;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(499, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 23);
            this.label7.TabIndex = 52;
            this.label7.Text = "Năm thành lập";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(765, 471);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(124, 41);
            this.button5.TabIndex = 58;
            this.button5.Text = "Thoát";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(590, 471);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(124, 41);
            this.button6.TabIndex = 57;
            this.button6.Text = "Xóa";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(410, 471);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(124, 41);
            this.button7.TabIndex = 56;
            this.button7.Text = "Sửa";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(234, 471);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(124, 41);
            this.button8.TabIndex = 55;
            this.button8.Text = "Thêm";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // dgvK
            // 
            this.dgvK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column18,
            this.Column19,
            this.Column20});
            this.dgvK.Location = new System.Drawing.Point(6, 215);
            this.dgvK.Name = "dgvK";
            this.dgvK.RowHeadersWidth = 51;
            this.dgvK.RowTemplate.Height = 24;
            this.dgvK.Size = new System.Drawing.Size(938, 231);
            this.dgvK.TabIndex = 59;
            this.dgvK.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvK_CellContentClick);
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Ma_Khoa";
            this.Column18.HeaderText = "Mã khoa";
            this.Column18.MinimumWidth = 6;
            this.Column18.Name = "Column18";
            this.Column18.Width = 125;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Ten_Khoa";
            this.Column19.HeaderText = "Tên khoa";
            this.Column19.MinimumWidth = 6;
            this.Column19.Name = "Column19";
            this.Column19.Width = 170;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "Nam_Thanh_Lap";
            this.Column20.HeaderText = "Năm thành lập";
            this.Column20.MinimumWidth = 6;
            this.Column20.Name = "Column20";
            this.Column20.Width = 125;
            // 
            // txbTKK
            // 
            this.txbTKK.Location = new System.Drawing.Point(191, 134);
            this.txbTKK.Name = "txbTKK";
            this.txbTKK.Size = new System.Drawing.Size(254, 30);
            this.txbTKK.TabIndex = 51;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(43, 141);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 23);
            this.label22.TabIndex = 50;
            this.label22.Text = "Tên khoa";
            // 
            // txbKK
            // 
            this.txbKK.Location = new System.Drawing.Point(191, 83);
            this.txbKK.Name = "txbKK";
            this.txbKK.Size = new System.Drawing.Size(254, 30);
            this.txbKK.TabIndex = 49;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(43, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(89, 23);
            this.label23.TabIndex = 48;
            this.label23.Text = "Mã khoa ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(341, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(239, 26);
            this.label24.TabIndex = 47;
            this.label24.Text = "DANH SÁCH KHOA ";
            // 
            // tabPage7
            // 
            this.tabPage7.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage7.Controls.Add(this.btnXemKH);
            this.tabPage7.Controls.Add(this.txbNKT);
            this.tabPage7.Controls.Add(this.label25);
            this.tabPage7.Controls.Add(this.btnThoatKH);
            this.tabPage7.Controls.Add(this.btnXoaKH);
            this.tabPage7.Controls.Add(this.btnSuaKH);
            this.tabPage7.Controls.Add(this.btnThemKH);
            this.tabPage7.Controls.Add(this.dgvKH);
            this.tabPage7.Controls.Add(this.txbNBD);
            this.tabPage7.Controls.Add(this.label26);
            this.tabPage7.Controls.Add(this.txbMKH);
            this.tabPage7.Controls.Add(this.label27);
            this.tabPage7.Controls.Add(this.label28);
            this.tabPage7.Location = new System.Drawing.Point(4, 32);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(950, 543);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Khóa học";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // btnXemKH
            // 
            this.btnXemKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemKH.Location = new System.Drawing.Point(62, 471);
            this.btnXemKH.Name = "btnXemKH";
            this.btnXemKH.Size = new System.Drawing.Size(124, 41);
            this.btnXemKH.TabIndex = 66;
            this.btnXemKH.Text = "Xem";
            this.btnXemKH.UseVisualStyleBackColor = true;
            this.btnXemKH.Click += new System.EventHandler(this.btnXemKH_Click);
            // 
            // txbNKT
            // 
            this.txbNKT.Location = new System.Drawing.Point(647, 83);
            this.txbNKT.Name = "txbNKT";
            this.txbNKT.Size = new System.Drawing.Size(254, 30);
            this.txbNKT.TabIndex = 65;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(499, 90);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 23);
            this.label25.TabIndex = 64;
            this.label25.Text = "Năm kết thúc";
            // 
            // btnThoatKH
            // 
            this.btnThoatKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoatKH.Location = new System.Drawing.Point(766, 471);
            this.btnThoatKH.Name = "btnThoatKH";
            this.btnThoatKH.Size = new System.Drawing.Size(124, 41);
            this.btnThoatKH.TabIndex = 70;
            this.btnThoatKH.Text = "Thoát";
            this.btnThoatKH.UseVisualStyleBackColor = true;
            this.btnThoatKH.Click += new System.EventHandler(this.btnThoatKH_Click);
            // 
            // btnXoaKH
            // 
            this.btnXoaKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaKH.Location = new System.Drawing.Point(587, 471);
            this.btnXoaKH.Name = "btnXoaKH";
            this.btnXoaKH.Size = new System.Drawing.Size(124, 41);
            this.btnXoaKH.TabIndex = 69;
            this.btnXoaKH.Text = "Xóa";
            this.btnXoaKH.UseVisualStyleBackColor = true;
            this.btnXoaKH.Click += new System.EventHandler(this.btnXoaKH_Click);
            // 
            // btnSuaKH
            // 
            this.btnSuaKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaKH.Location = new System.Drawing.Point(413, 471);
            this.btnSuaKH.Name = "btnSuaKH";
            this.btnSuaKH.Size = new System.Drawing.Size(124, 41);
            this.btnSuaKH.TabIndex = 68;
            this.btnSuaKH.Text = "Sửa";
            this.btnSuaKH.UseVisualStyleBackColor = true;
            this.btnSuaKH.Click += new System.EventHandler(this.btnSuaKH_Click);
            // 
            // btnThemKH
            // 
            this.btnThemKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemKH.Location = new System.Drawing.Point(232, 471);
            this.btnThemKH.Name = "btnThemKH";
            this.btnThemKH.Size = new System.Drawing.Size(124, 41);
            this.btnThemKH.TabIndex = 67;
            this.btnThemKH.Text = "Thêm";
            this.btnThemKH.UseVisualStyleBackColor = true;
            this.btnThemKH.Click += new System.EventHandler(this.btnThemKH_Click);
            // 
            // dgvKH
            // 
            this.dgvKH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column21,
            this.Column22,
            this.Column23});
            this.dgvKH.Location = new System.Drawing.Point(6, 215);
            this.dgvKH.Name = "dgvKH";
            this.dgvKH.RowHeadersWidth = 51;
            this.dgvKH.RowTemplate.Height = 24;
            this.dgvKH.Size = new System.Drawing.Size(938, 231);
            this.dgvKH.TabIndex = 71;
            this.dgvKH.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKH_CellContentClick);
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "Ma_Khoa_Hoc";
            this.Column21.HeaderText = "Mã khóa học";
            this.Column21.MinimumWidth = 6;
            this.Column21.Name = "Column21";
            this.Column21.Width = 125;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "Nam_Bat_Dau";
            this.Column22.HeaderText = "Năm bắt đầu";
            this.Column22.MinimumWidth = 6;
            this.Column22.Name = "Column22";
            this.Column22.Width = 125;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "Nam_Ket_Thuc";
            this.Column23.HeaderText = "Năm kết thúc";
            this.Column23.MinimumWidth = 6;
            this.Column23.Name = "Column23";
            this.Column23.Width = 125;
            // 
            // txbNBD
            // 
            this.txbNBD.Location = new System.Drawing.Point(191, 134);
            this.txbNBD.Name = "txbNBD";
            this.txbNBD.Size = new System.Drawing.Size(254, 30);
            this.txbNBD.TabIndex = 63;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(43, 141);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(117, 23);
            this.label26.TabIndex = 62;
            this.label26.Text = "Năm bắt đầu";
            // 
            // txbMKH
            // 
            this.txbMKH.Location = new System.Drawing.Point(191, 83);
            this.txbMKH.Name = "txbMKH";
            this.txbMKH.Size = new System.Drawing.Size(254, 30);
            this.txbMKH.TabIndex = 61;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(43, 90);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(119, 23);
            this.label27.TabIndex = 60;
            this.label27.Text = "Mã khóa học";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(341, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(294, 26);
            this.label28.TabIndex = 59;
            this.label28.Text = "DANH SÁCH KHÓA HỌC";
            // 
            // tabPage8
            // 
            this.tabPage8.BackgroundImage = global::AppQuanLyDiemSV.Properties.Resources.istockphoto_1166323937_612x612;
            this.tabPage8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage8.Controls.Add(this.btnXemGD);
            this.tabPage8.Controls.Add(this.txbSTC);
            this.tabPage8.Controls.Add(this.label37);
            this.tabPage8.Controls.Add(this.txbSTTH);
            this.tabPage8.Controls.Add(this.label36);
            this.tabPage8.Controls.Add(this.txbSTLT);
            this.tabPage8.Controls.Add(this.label35);
            this.tabPage8.Controls.Add(this.txbHK);
            this.tabPage8.Controls.Add(this.label29);
            this.tabPage8.Controls.Add(this.txbNH);
            this.tabPage8.Controls.Add(this.label30);
            this.tabPage8.Controls.Add(this.txbMHHGD);
            this.tabPage8.Controls.Add(this.label31);
            this.tabPage8.Controls.Add(this.btnThoatGD);
            this.tabPage8.Controls.Add(this.btnXoaGD);
            this.tabPage8.Controls.Add(this.btnSuaGD);
            this.tabPage8.Controls.Add(this.btnThemGD);
            this.tabPage8.Controls.Add(this.dgvGD);
            this.tabPage8.Controls.Add(this.txbMKGD);
            this.tabPage8.Controls.Add(this.label32);
            this.tabPage8.Controls.Add(this.txbMCT);
            this.tabPage8.Controls.Add(this.label33);
            this.tabPage8.Controls.Add(this.label34);
            this.tabPage8.Location = new System.Drawing.Point(4, 32);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(950, 543);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Giảng dạy";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // btnXemGD
            // 
            this.btnXemGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemGD.Location = new System.Drawing.Point(61, 478);
            this.btnXemGD.Name = "btnXemGD";
            this.btnXemGD.Size = new System.Drawing.Size(124, 41);
            this.btnXemGD.TabIndex = 33;
            this.btnXemGD.Text = "Xem";
            this.btnXemGD.UseVisualStyleBackColor = true;
            this.btnXemGD.Click += new System.EventHandler(this.btnXemGD_Click);
            // 
            // txbSTC
            // 
            this.txbSTC.Location = new System.Drawing.Point(647, 187);
            this.txbSTC.Name = "txbSTC";
            this.txbSTC.Size = new System.Drawing.Size(254, 30);
            this.txbSTC.TabIndex = 32;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(499, 194);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(90, 23);
            this.label37.TabIndex = 31;
            this.label37.Text = "Số tín chỉ";
            // 
            // txbSTTH
            // 
            this.txbSTTH.Location = new System.Drawing.Point(647, 145);
            this.txbSTTH.Name = "txbSTTH";
            this.txbSTTH.Size = new System.Drawing.Size(254, 30);
            this.txbSTTH.TabIndex = 30;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(487, 152);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(154, 23);
            this.label36.TabIndex = 29;
            this.label36.Text = "Số tiết thực hành";
            // 
            // txbSTLT
            // 
            this.txbSTLT.Location = new System.Drawing.Point(647, 98);
            this.txbSTLT.Name = "txbSTLT";
            this.txbSTLT.Size = new System.Drawing.Size(254, 30);
            this.txbSTLT.TabIndex = 28;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(499, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(139, 23);
            this.label35.TabIndex = 27;
            this.label35.Text = "Số tiết lý thuyết";
            // 
            // txbHK
            // 
            this.txbHK.Location = new System.Drawing.Point(647, 56);
            this.txbHK.Name = "txbHK";
            this.txbHK.Size = new System.Drawing.Size(254, 30);
            this.txbHK.TabIndex = 26;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(499, 63);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(68, 23);
            this.label29.TabIndex = 25;
            this.label29.Text = "Học kỳ";
            // 
            // txbNH
            // 
            this.txbNH.Location = new System.Drawing.Point(191, 187);
            this.txbNH.Name = "txbNH";
            this.txbNH.Size = new System.Drawing.Size(254, 30);
            this.txbNH.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(43, 194);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 23);
            this.label30.TabIndex = 23;
            this.label30.Text = "Năm học";
            // 
            // txbMHHGD
            // 
            this.txbMHHGD.Location = new System.Drawing.Point(191, 145);
            this.txbMHHGD.Name = "txbMHHGD";
            this.txbMHHGD.Size = new System.Drawing.Size(254, 30);
            this.txbMHHGD.TabIndex = 22;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(43, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 23);
            this.label31.TabIndex = 21;
            this.label31.Text = "Mã môn học";
            // 
            // btnThoatGD
            // 
            this.btnThoatGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoatGD.Location = new System.Drawing.Point(767, 478);
            this.btnThoatGD.Name = "btnThoatGD";
            this.btnThoatGD.Size = new System.Drawing.Size(124, 41);
            this.btnThoatGD.TabIndex = 37;
            this.btnThoatGD.Text = "Thoát";
            this.btnThoatGD.UseVisualStyleBackColor = true;
            this.btnThoatGD.Click += new System.EventHandler(this.btnThoatGD_Click);
            // 
            // btnXoaGD
            // 
            this.btnXoaGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaGD.Location = new System.Drawing.Point(588, 478);
            this.btnXoaGD.Name = "btnXoaGD";
            this.btnXoaGD.Size = new System.Drawing.Size(124, 41);
            this.btnXoaGD.TabIndex = 36;
            this.btnXoaGD.Text = "Xóa";
            this.btnXoaGD.UseVisualStyleBackColor = true;
            this.btnXoaGD.Click += new System.EventHandler(this.btnXoaGD_Click);
            // 
            // btnSuaGD
            // 
            this.btnSuaGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaGD.Location = new System.Drawing.Point(418, 478);
            this.btnSuaGD.Name = "btnSuaGD";
            this.btnSuaGD.Size = new System.Drawing.Size(124, 41);
            this.btnSuaGD.TabIndex = 35;
            this.btnSuaGD.Text = "Sửa";
            this.btnSuaGD.UseVisualStyleBackColor = true;
            this.btnSuaGD.Click += new System.EventHandler(this.btnSuaGD_Click);
            // 
            // btnThemGD
            // 
            this.btnThemGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemGD.Location = new System.Drawing.Point(230, 478);
            this.btnThemGD.Name = "btnThemGD";
            this.btnThemGD.Size = new System.Drawing.Size(124, 41);
            this.btnThemGD.TabIndex = 34;
            this.btnThemGD.Text = "Thêm";
            this.btnThemGD.UseVisualStyleBackColor = true;
            this.btnThemGD.Click += new System.EventHandler(this.btnThemGD_Click);
            // 
            // dgvGD
            // 
            this.dgvGD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31});
            this.dgvGD.Location = new System.Drawing.Point(6, 229);
            this.dgvGD.Name = "dgvGD";
            this.dgvGD.RowHeadersWidth = 51;
            this.dgvGD.RowTemplate.Height = 24;
            this.dgvGD.Size = new System.Drawing.Size(938, 231);
            this.dgvGD.TabIndex = 38;
            this.dgvGD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGD_CellContentClick);
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "Ma_CT";
            this.Column24.HeaderText = "Mã chương trình";
            this.Column24.MinimumWidth = 6;
            this.Column24.Name = "Column24";
            this.Column24.Width = 80;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "Ma_Khoa";
            this.Column25.HeaderText = "Mã khoa";
            this.Column25.MinimumWidth = 6;
            this.Column25.Name = "Column25";
            this.Column25.Width = 80;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "MaMH";
            this.Column26.HeaderText = "Mã môn học";
            this.Column26.MinimumWidth = 6;
            this.Column26.Name = "Column26";
            this.Column26.Width = 80;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "Nam_Hoc";
            this.Column27.HeaderText = "Năm học";
            this.Column27.MinimumWidth = 6;
            this.Column27.Name = "Column27";
            this.Column27.Width = 80;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "Hoc_Ky";
            this.Column28.HeaderText = "Học kỳ";
            this.Column28.MinimumWidth = 6;
            this.Column28.Name = "Column28";
            this.Column28.Width = 80;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "STLT";
            this.Column29.HeaderText = "Số tiết lý thuyết";
            this.Column29.MinimumWidth = 6;
            this.Column29.Name = "Column29";
            this.Column29.Width = 80;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "STTH";
            this.Column30.HeaderText = "Số tiết thực hành";
            this.Column30.MinimumWidth = 6;
            this.Column30.Name = "Column30";
            this.Column30.Width = 80;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "So_Tin_Chi";
            this.Column31.HeaderText = "Số tín chỉ";
            this.Column31.MinimumWidth = 6;
            this.Column31.Name = "Column31";
            this.Column31.Width = 80;
            // 
            // txbMKGD
            // 
            this.txbMKGD.Location = new System.Drawing.Point(191, 98);
            this.txbMKGD.Name = "txbMKGD";
            this.txbMKGD.Size = new System.Drawing.Size(254, 30);
            this.txbMKGD.TabIndex = 20;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(43, 105);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(84, 23);
            this.label32.TabIndex = 19;
            this.label32.Text = "Mã khoa";
            // 
            // txbMCT
            // 
            this.txbMCT.Location = new System.Drawing.Point(191, 56);
            this.txbMCT.Name = "txbMCT";
            this.txbMCT.Size = new System.Drawing.Size(254, 30);
            this.txbMCT.TabIndex = 18;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(31, 63);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(154, 23);
            this.label33.TabIndex = 17;
            this.label33.Text = "Mã chương trình";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(394, 17);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(148, 26);
            this.label34.TabIndex = 16;
            this.label34.Text = "GIẢNG DẠY";
            // 
            // txbTimSV
            // 
            this.txbTimSV.Location = new System.Drawing.Point(647, 185);
            this.txbTimSV.Name = "txbTimSV";
            this.txbTimSV.Size = new System.Drawing.Size(254, 30);
            this.txbTimSV.TabIndex = 18;
            this.txbTimSV.TextChanged += new System.EventHandler(this.txbTimSV_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(504, 192);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(124, 23);
            this.label42.TabIndex = 19;
            this.label42.Text = "Tìm sinh viên";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(500, 191);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(107, 23);
            this.label43.TabIndex = 37;
            this.label43.Text = "Tìm mã lớp";
            // 
            // txbTimlop
            // 
            this.txbTimlop.Location = new System.Drawing.Point(648, 184);
            this.txbTimlop.Name = "txbTimlop";
            this.txbTimlop.Size = new System.Drawing.Size(254, 30);
            this.txbTimlop.TabIndex = 36;
            this.txbTimlop.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(499, 135);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(117, 23);
            this.label44.TabIndex = 49;
            this.label44.Text = "Tìm mã môn";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(647, 128);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(254, 30);
            this.textBox1.TabIndex = 48;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // fQuanLyDiemSinhVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 603);
            this.Controls.Add(this.tabDiem);
            this.Name = "fQuanLyDiemSinhVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm quản lý điểm sinh viên";
            this.Load += new System.EventHandler(this.fQuanLyDiemSinhVien_Load);
            this.tabDiem.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSinhVien)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLop)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMH)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBD)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvK)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKH)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabDiem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvSinhVien;
        private System.Windows.Forms.TextBox txbTenSV;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbMSVSV;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnThoatSV;
        private System.Windows.Forms.Button btnXoaSV;
        private System.Windows.Forms.Button btnSuaSV;
        private System.Windows.Forms.Button btnThemSV;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbDanToc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbNS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbML;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbSTTL;
        private System.Windows.Forms.Label txbSTT;
        private System.Windows.Forms.TextBox txbCTH;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbMaKhoa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnThoatLop;
        private System.Windows.Forms.Button btnXoaLop;
        private System.Windows.Forms.Button btnSuaLop;
        private System.Windows.Forms.Button btnThemLop;
        private System.Windows.Forms.DataGridView dgvLop;
        private System.Windows.Forms.TextBox txbMK;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbML1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbTMH;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnThoatMH;
        private System.Windows.Forms.Button btnXoaMH;
        private System.Windows.Forms.Button btnSuaMH;
        private System.Windows.Forms.Button btnThemMH;
        private System.Windows.Forms.DataGridView dgvMH;
        private System.Windows.Forms.TextBox txbMKMH;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txbMMH;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txbDiemthiBD;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txbLTBD;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dgvBD;
        private System.Windows.Forms.TextBox txbMaMHBD;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txbMSVBD;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox txbNTL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView dgvK;
        private System.Windows.Forms.TextBox txbTKK;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txbKK;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TextBox txbNKT;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnThoatKH;
        private System.Windows.Forms.Button btnXoaKH;
        private System.Windows.Forms.Button btnSuaKH;
        private System.Windows.Forms.Button btnThemKH;
        private System.Windows.Forms.DataGridView dgvKH;
        private System.Windows.Forms.TextBox txbNBD;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txbMKH;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txbSTC;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txbSTTH;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txbSTLT;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txbHK;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txbNH;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txbMHHGD;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnThoatGD;
        private System.Windows.Forms.Button btnXoaGD;
        private System.Windows.Forms.Button btnSuaGD;
        private System.Windows.Forms.Button btnThemGD;
        private System.Windows.Forms.DataGridView dgvGD;
        private System.Windows.Forms.TextBox txbMKGD;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txbMCT;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnXem;
        private System.Windows.Forms.Button btnXemL;
        private System.Windows.Forms.Button btnXemMH;
        private System.Windows.Forms.Button btnXemSV;
        private System.Windows.Forms.Button btnXemK;
        private System.Windows.Forms.Button btnXemKH;
        private System.Windows.Forms.Button btnXemGD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.TextBox txbTimSV;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txbTimlop;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox1;
    }
}